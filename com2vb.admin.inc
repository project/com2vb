<?php

/**
 * @file
 * Admin page callbacks for the com2vb module.
 */

/**
 * Menu callback; present an administrative comment listing.
 */
function com2vb_admin($type = 'new') {
  $edit = $_POST;

  if (isset($edit['operation']) && ($edit['operation'] == 'delete') && isset($edit['comments']) && $edit['comments']) {
    return drupal_get_form('com2vb_multiple_delete_confirm');
  }
  else {
    return drupal_get_form('com2vb_admin_overview', $type, arg(4));
  }
}

/**
 * Form builder; Builds the comment overview form for the admin.
 *
 * @param $type
 *   Not used.
 * @param $arg
 *   Current path's fourth component deciding the form type (Published comments/Approval queue)
 * @return
 *   The form structure.
 * @ingroup forms
 * @see com2vb_admin_overview_validate()
 * @see com2vb_admin_overview_submit()
 * @see theme_com2vb_admin_overview()
 */
function com2vb_admin_overview($type = 'new', $arg) {
  // build an 'Update options' form
  $form['options'] = array(
    '#type' => 'fieldset', '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">', '#suffix' => '</div>'
  );
  $options = array();
  foreach (com2vb_operations($arg == 'approval' ? 'publish' : 'unpublish') as $key => $value) {
    $options[$key] = $value[0];
  }
  $form['options']['operation'] = array('#type' => 'select', '#options' => $options, '#default_value' => 'publish');
  $form['options']['submit'] = array('#type' => 'submit', '#value' => t('Update'));

  // load the comments that we want to display
  $status = ($arg == 'approval') ? COMMENT_NOT_PUBLISHED : COMMENT_PUBLISHED;
  $form['header'] = array('#type' => 'value', '#value' => array(
    theme('table_select_header_cell'),
    array('data' => t('Subject'), 'field' => 'subject'),
    array('data' => t('Author'), 'field' => 'name'),
    array('data' => t('Posted in'), 'field' => 'node_title'),
    array('data' => t('Time'), 'field' => 'timestamp', 'sort' => 'desc'),
    array('data' => t('Source'), 'field' => 'drcid')
  ));
  $result = pager_query('SELECT p.title AS subject, vbt.dr_nid AS nid, p.postid AS cid, p.pagetext AS comment, p.dateline AS timestamp, p.visible AS status, p.username AS name, u.name AS registered_name, u.uid, c.cid AS drcid FROM {vb_thread} vbt, '._vb_prefix().'post p LEFT JOIN {vb_users} vbu ON vbu.vb_userid = p.userid LEFT JOIN {users} u ON u.uid = vbu.dr_userid, '._vb_prefix().'post p1 LEFT JOIN {comments} c ON c.cid = p1.postid WHERE p.threadid = vbt.vb_thread AND p.parentid > 0 AND  p.visible = %d AND p1.postid = p.postid'. tablesort_sql($form['header']['#value']), 50, 0, NULL, !$status);

  // build a table listing the appropriate comments
  $destination = drupal_get_destination();
  while ($comment = db_fetch_object($result)) {
    $comments[$comment->cid] = '';
    $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
    $form['subject'][$comment->cid] = array('#value' => l($comment->subject, 'node/'. $comment->nid, array('attributes' => array('title' => truncate_utf8($comment->comment, 128)), 'fragment' => 'comment-'. $comment->cid)));
    $form['username'][$comment->cid] = array('#value' => theme('username', $comment));
    $form['node_title'][$comment->cid] = array('#value' => l($comment->node_title, 'node/'. $comment->nid));
    $form['timestamp'][$comment->cid] = array('#value' => format_date($comment->timestamp, 'small'));
	if ($comment->drcid > 0) {
    	$form['source'][$comment->cid] = array('#value' => t('Drupal'));
		}
		else {
		$form['source'][$comment->cid] = array('#value' => t('vBulletin'));
		}
  }
  $form['comments'] = array('#type' => 'checkboxes', '#options' => isset($comments) ? $comments: array());
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  return $form;
}

/**
 * Validate com2vb_admin_overview form submissions.
 *
 * We can't execute any 'Update options' if no comments were selected.
 */
function com2vb_admin_overview_validate($form, &$form_state) {
  $form_state['values']['comments'] = array_diff($form_state['values']['comments'], array(0));
  if (count($form_state['values']['comments']) == 0) {
    form_set_error('', t('Please select one or more comments to perform the update on.'));
    drupal_goto('admin/content/comment');
  }
}

/**
 * Process com2vb_admin_overview form submissions.
 *
 * Execute the chosen 'Update option' on the selected comments, such as
 * publishing, unpublishing or deleting.
 */
function com2vb_admin_overview_submit($form, &$form_state) {
  $operations = com2vb_operations();
  if ($operations[$form_state['values']['operation']][1]) {
    // extract the appropriate database query operation
    $query = $operations[$form_state['values']['operation']][1];
    foreach ($form_state['values']['comments'] as $cid => $value) {
      if ($value) {
        // perform the update action, then refresh node statistics
        db_query($query, $cid);
        $comment = _com2vb_load($cid);
        _com2vb_update_node_statistics($comment->nid);
        // Allow modules to respond to the updating of a comment.
        com2vb_invoke_comment($comment, $form_state['values']['operation']);
        // Add an entry to the watchdog log.
        watchdog('content', 'Comment: updated %subject.', array('%subject' => $comment->subject), WATCHDOG_NOTICE, l(t('view'), 'node/'. $comment->nid, array('fragment' => 'comment-'. $comment->cid)));
      }
    }
    cache_clear_all();
    drupal_set_message(t('The update has been performed.'));
    $form_state['redirect'] = 'admin/content/comment';
  }
}

/**
 * Theme the comment admin form.
 *
 * @param $form
 *   An associative array containing the structure of the form.
 * @ingroup themeable
 */
function theme_com2vb_admin_overview($form) {
  $output = drupal_render($form['options']);
  if (isset($form['subject']) && is_array($form['subject'])) {
    foreach (element_children($form['subject']) as $key) {
      $row = array();
      $row[] = drupal_render($form['comments'][$key]);
      $row[] = drupal_render($form['subject'][$key]);
      $row[] = drupal_render($form['username'][$key]);
      $row[] = drupal_render($form['node_title'][$key]);
      $row[] = drupal_render($form['timestamp'][$key]);
      $row[] = drupal_render($form['source'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No comments available.'), 'colspan' => '6'));
  }

  $output .= theme('table', $form['header']['#value'], $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

function com2vb_admin_vbsettings() {
  $form['forum_settings'] = array(
    '#type' => 'fieldset',
	'#title' => t('vBulletin\'s options'),
    '#collapsible' => TRUE,
  );
  
  $form['forum_settings']['com2vb_db_prefix'] = array(
    '#type' => 'textfield',
	'#title' => t('Database prefix for vBulletin'),
	'#default_value' => variable_get('com2vb_db_prefix', COM2VB_PREFIX),
	'#size' => 16,
	'#maxlength' => 16,
	'#description' => t('Prefix for table names'),
  );

  $form['forum_settings']['com2vb_forum_url'] = array(
    '#type' => 'textfield',
	'#title' => t('URL to vBulletin\'s forum'),
	'#default_value' => variable_get('com2vb_forum_url', COM2VB_FORUM_URL),
	'#description' => t('This URL will be used in links to forum\'s threads in nodes. [Example: URL http://www.example.com/forum/ will be used in building of link http://www.example.com/forum/showthread.php?t=124 to full discussion of node]'),
  );  
  $form['forum_settings']['com2vb_def_forum'] = array(
    '#type' => 'select',
	'#title' => t('Default forum to post comments'),
	'#default_value' => variable_get('com2vb_def_forum', COM2VB_FORUM_POST),
	'#options' => _vb_forum_list(),
	'#size' => 5,
	'#maxlength' => 16,
	'#multiple' => FALSE,
  );  

  $form['forum_settings']['com2vb_html_conv'] = array(
    '#type' => 'checkbox',
	'#title' => t('Convert HTML tags in comments to BBCode'),
	'#default_value' => variable_get('com2vb_html_conv', COM2VB_HTML_CONV),
  );

  return system_settings_form($form);
}

/**
 * Menu callback; presents the linked users settings page.
 */
function com2vb_admin_linkedusers() {
  $form['users_list'] = array(
    '#type' => 'fieldset',
	'#title' => t('Crossed users'),
	'#description' => t('List of linked users, who have accounts in Drupal and vBulletin. To add a new linked user click ' . l(t('here'), 'admin/content/comment/linkedusers/add')),
    '#collapsible' => TRUE,
  );

$form['header'] = array('#type' => 'value', '#value' => array(
	theme('table_select_header_cell'),
    array('data' => t('Drupal Id'), 'field' => 'dr_userid', 'sort' => 'asc'),
    array('data' => t('Drupal name'), 'field' => 'dr_name'),
    array('data' => t('vBulletin Id'), 'field' => 'vb_userid'),
    array('data' => t('vBulletin name'), 'field' => 'vb_name')
  ));  

	$result =  db_query('SELECT dru.uid AS dr_userid, dru.name AS dr_name, vbu.userid AS vb_userid, vbu.username AS vb_name FROM {users} dru, {vb_users} drvb, '._vb_prefix().'user vbu WHERE drvb.dr_userid = dru.uid AND vbu.userid = drvb.vb_userid '. tablesort_sql($form['header']['#value']));

while ($user = db_fetch_object($result)) {
	$users[$user->dr_userid] = '';
	$form['dr_userid'][$user->dr_userid] = array('#value' => $user->dr_userid);
	$form['dr_name'][$user->dr_userid] = array('#value' => $user->dr_name);
	$form['vb_userid'][$user->dr_userid] = array('#value' => $user->vb_userid);
	$form['vb_name'][$user->dr_userid] = array('#value' => $user->vb_name); 
	}
	$form['users'] = array('#type' => 'checkboxes', '#options' => $users);
	
  $form['submit'] = array(
   	'#type' => 'submit',
	'#value' => t('Delete selected users'),
	);
	
	return $form;
}
/**
 * Menu callback; presents the threads in forum with comments from site.
 */
function com2vb_admin_threads() {
  $form['thread_list'] = array(
    '#type' => 'fieldset',
	'#title' => t('Linked threads'),
	'#description' => t('Below is a list of linked threads in forum with nodes from site. You can unlink it from nodes. Unlinking threads with nodes does not delete anything - node become uncommented and thread in forum become "independent" from node.'),
    '#collapsible' => TRUE,
  );

$form['header'] = array('#type' => 'value', '#value' => array(
	theme('table_select_header_cell'),
    array('data' => t('Title'), 'field' => 'nid'),
    array('data' => t('Published'), 'field' => 'drtime', 'sort' => 'desc'),
    array('data' => t('vb thread'), 'field' => 'threadid'),
    array('data' => t('Total comments'), 'field' => 'replycount'),
	array('data' => t('Last comment'), 'field' => 'lastpost'),
  ));  

	$result =  db_query('SELECT n.nid, n.title AS drtitle, n.changed AS drtime, frt.threadid, frt.title AS vbtitle, frt.replycount, frt.lastpost FROM {node} n LEFT JOIN {vb_thread} vbt ON vbt.dr_nid = n.nid LEFT JOIN '._vb_prefix().'thread frt ON frt.threadid = vbt.vb_thread WHERE vbt.dr_nid > 0 AND n.status = 1 '. tablesort_sql($form['header']['#value']));

while ($thread = db_fetch_object($result)) {
	$threads[$thread->nid] = '';
	$form['node'][$thread->nid] = array('#value' => l($thread->drtitle, 'node/'. $thread->nid));
	$form['node_time'][$thread->nid] = array('#value' => format_date($thread->drtime, 'small'));
	$form['thread'][$thread->nid] = array('#value' => l($thread->vbtitle, variable_get('com2vb_forum_url', COM2VB_FORUM_URL).'showthread.php?goto=newpost&t='. $thread->threadid));
	$form['thread_count'][$thread->nid] = array('#value' => $thread->replycount); 
	$form['thread_lastpost'][$thread->nid] = array('#value' => format_date($thread->lastpost, 'small'));
	}
	$form['threads'] = array('#type' => 'checkboxes', '#options' => $threads);
	
  $form['submit'] = array(
   	'#type' => 'submit',
	'#value' => t('Unlink selected threads'),
	);
	
	return $form;
}
/**
 * We can't delete linked users if no users were selected.
 */
function com2vb_admin_linkedusers_validate($form_id, $form_values) {
  $form_values['users'] = array_diff($form_values['users'], array(0));
  if (count($form_values['users']) == 0) {
    form_set_error('', t('Please select one or more users to delete.'));
    drupal_goto('admin/content/comment/linkedusers');
  }
}
/**
 * We can't unlink threads if no threads were selected.
 */
function com2vb_admin_threads_validate($form_id, $form_values) {
  $form_values['threads'] = array_diff($form_values['threads'], array(0));
  if (count($form_values['threads']) == 0) {
    form_set_error('', t('Please select one or more threads to unlink.'));
    drupal_goto('admin/content/comment/threads');
  }
}
/**
 * Delete selected linked users from cross table
 */
function com2vb_admin_linkedusers_submit($form_id, $form_values) {

    foreach ($form_values['users'] as $dr_userid => $value) {
      if ($value) {
        // perform the update action, then refresh node statistics
        db_query('DELETE FROM {vb_users} WHERE dr_userid = %d', $dr_userid);
      }
    }
    cache_clear_all();
    drupal_set_message(t('Cross users has been deleted.'));
    return 'admin/content/comment/linkedusers';

}
/**
 * Delete selected linked users from cross table
 */
function com2vb_admin_threads_submit($form_id, $form_values) {

    foreach ($form_values['threads'] as $dr_nid => $value) {
      if ($value) {
        db_query('DELETE FROM {vb_thread} WHERE dr_nid = %d', $dr_nid);
      }
    }
    cache_clear_all();
    drupal_set_message(t('Threads has been unlinked.'));
    return 'admin/content/comment/threads';

}

function theme_com2vb_admin_linkedusers($form) {	
$output = drupal_render($form['users_list']);

if (isset($form['dr_userid']) && is_array($form['dr_userid'])) {
	foreach (element_children($form['dr_userid']) as $key) {
      $row = array();
	  $row[] = drupal_render($form['users'][$key]);
      $row[] = drupal_render($form['dr_userid'][$key]);
      $row[] = drupal_render($form['dr_name'][$key]);
      $row[] = drupal_render($form['vb_userid'][$key]);
      $row[] = drupal_render($form['vb_name'][$key]);
	  $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No linked users available.'), 'colspan' => '5'));
  }

	$output .= theme('table', $form['header']['#value'], $rows);
	$output .= drupal_render($form);
	
	return $output;
}

function theme_com2vb_admin_threads($form) {	
$output = drupal_render($form['thread_list']);

if (isset($form['node']) && is_array($form['node'])) {
	foreach (element_children($form['node']) as $key) {
      $row = array();
	  $row[] = drupal_render($form['threads'][$key]);
	  $row[] = drupal_render($form['node'][$key]);
      $row[] = drupal_render($form['node_time'][$key]);
      $row[] = drupal_render($form['thread'][$key]);
      $row[] = drupal_render($form['thread_count'][$key]);
      $row[] = drupal_render($form['thread_lastpost'][$key]);
	  $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No linked threads available.'), 'colspan' => '5'));
  }

	$output .= theme('table', $form['header']['#value'], $rows);
	$output .= drupal_render($form);
	
	return $output;
}
/**
 * Menu callback; add linked users page.
 */
function com2vb_admin_linkedusers_new() {
  $form['users_new'] = array(
    '#type' => 'fieldset',
	'#title' => t('Add new linked user'),
    '#collapsible' => FALSE,
  ); 
  
  $dr_user = array();
  $dr_users = db_query('SELECT dr.uid, dr.name FROM {users} dr LEFT JOIN {vb_users} vbu ON vbu.dr_userid = dr.uid WHERE vbu.dr_userid IS NULL AND dr.uid > 0 ORDER BY dr.name');
  while ($user1 = db_fetch_object($dr_users)) {
  	$dr_user[$user1->uid] = $user1->name;
	}
  
  $form['users_new']['dr_user'] = array(
  	'#type' => 'select',
	'#title' => t('User from Drupal'),
	'#options' => $dr_user
	);

  $vb_user = array();
  $vb_users = db_query('SELECT fr.userid, fr.username FROM '._vb_prefix().'user fr LEFT JOIN {vb_users} vbu ON vbu.vb_userid = fr.userid WHERE vbu.vb_userid IS NULL ORDER BY fr.username');
  while ($user2 = db_fetch_object($vb_users)) {
  	$vb_user[$user2->userid] = $user2->username;
	}
  
  $form['users_new']['vb_user'] = array(
  	'#type' => 'select',
	'#title' => t('User from vBulletin'),
	'#options' => $vb_user
	);
 
   $form['users_new']['submit'] = array(
   	'#type' => 'submit',
	'#value' => t('Add new linked user'),
	);
 
  return $form;
}

function com2vb_admin_linkedusers_new_submit($form_id, $form_values) {

	$dr_cross = $form_values['dr_user'];
	$vb_cross = $form_values['vb_user'];
	
	if ($dr_cross && $vb_cross) {
		db_query('INSERT INTO {vb_users}(dr_userid, vb_userid) VALUES(%d, %d)', $dr_cross, $vb_cross);
		}
		
	return 'admin/content/comment/linkedusers';
}

/**
 * List the selected comments and verify that the admin really wants to delete
 * them.
 *
 * @param $form_state
 *   An associative array containing the current state of the form.
 * @return
 *   TRUE if the comments should be deleted, FALSE otherwise.
 * @ingroup forms
 * @see comment_multiple_delete_confirm_submit()
 */
function com2vb_multiple_delete_confirm(&$form_state) {
  $edit = $form_state['post'];

  $form['comments'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter() returns only elements with actual values
  $comment_counter = 0;
  foreach (array_filter($edit['comments']) as $cid => $value) {
    $comment = _com2vb_load($cid);
    if (is_object($comment) && is_numeric($comment->cid)) {
      $subject = db_result(db_query('SELECT title AS subject FROM '._vb_prefix().'post WHERE postid = %d', $cid));
      $form['comments'][$cid] = array('#type' => 'hidden', '#value' => $cid, '#prefix' => '<li>', '#suffix' => check_plain($subject) .'</li>');
      $comment_counter++;
    }
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');

  if (!$comment_counter) {
    drupal_set_message(t('There do not appear to be any comments to delete or your selected comment was deleted by another administrator.'));
    drupal_goto('admin/content/comment');
  }
  else {
    return confirm_form($form,
                        t('Are you sure you want to delete these comments and all their children?'),
                        'admin/content/comment', t('This action cannot be undone.'),
                        t('Delete comments'), t('Cancel'));
  }
}

/**
 * Process com2vb_multiple_delete_confirm form submissions.
 *
 * Perform the actual comment deletion.
 */
function com2vb_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['comments'] as $cid => $value) {
      $comment = _com2vb_load($cid);
      _com2vb_delete_thread($comment);
      _com2vb_update_node_statistics($comment->nid);
    }
    cache_clear_all();
    drupal_set_message(t('The comments have been deleted.'));
  }
  $form_state['redirect'] = 'admin/content/comment';
}

/**
 * Menu callback; delete a comment.
 *
 * @param $cid
 *   The comment do be deleted.
 */
function com2vb_delete($cid = NULL) {
  $comment = db_fetch_object(db_query('SELECT c.*, u.name AS registered_name, u.uid FROM {comments} c INNER JOIN {users} u ON u.uid = c.uid WHERE c.cid = %d', $cid));
  $comment->name = $comment->uid ? $comment->registered_name : $comment->name;

  $output = '';

  if (is_object($comment) && is_numeric($comment->cid)) {
    $output = drupal_get_form('com2vb_confirm_delete', $comment);
  }
  else {
    drupal_set_message(t('The comment no longer exists.'));
  }

  return $output;
}

/**
 * Form builder; Builds the confirmation form for deleting a single comment.
 *
 * @ingroup forms
 * @see comment_confirm_delete_submit()
 */
function com2vb_confirm_delete(&$form_state, $comment) {
  $form = array();
  $form['#comment'] = $comment;
  return confirm_form(
    $form,
    t('Are you sure you want to delete the comment %title?', array('%title' => $comment->subject)),
    'node/'. $comment->nid,
    t('Any replies to this comment will be lost. This action cannot be undone.'),
    t('Delete'),
    t('Cancel'),
    'comment_confirm_delete');
}

/**
 * Process com2vb_confirm_delete form submissions.
 */
function com2vb_confirm_delete_submit($form, &$form_state) {
  drupal_set_message(t('The comment and all its replies have been deleted.'));

  $comment = $form['#comment'];

  // Delete comment and its replies.
  _com2vb_delete_thread($comment);

  _com2vb_update_node_statistics($comment->nid);

  // Clear the cache so an anonymous user sees that his comment was deleted.
  cache_clear_all();

  $form_state['redirect'] = "node/$comment->nid";
}

/**
 * Perform the actual deletion of a comment and all its replies.
 *
 * @param $comment
 *   An associative array describing the comment to be deleted.
 */
function _com2vb_delete_thread($comment) {
  if (!is_object($comment) || !is_numeric($comment->cid)) {
    watchdog('content', 'Cannot delete non-existent comment.', array(), WATCHDOG_WARNING);
    return;
  }

  // Delete the comment:
  db_query('DELETE FROM {comments} WHERE cid = %d', $comment->cid);
  db_query('DELETE FROM '._vb_prefix().' WHERE postid = %d', $comment->cid);
  watchdog('content', 'Comment: deleted %subject.', array('%subject' => $comment->subject));

  com2vb_invoke_comment($comment, 'delete');

  // Delete the comment's replies
  $result = db_query('SELECT c.*, u.name AS registered_name, u.uid FROM {comments} c INNER JOIN {users} u ON u.uid = c.uid WHERE pid = %d', $comment->cid);
  while ($comment = db_fetch_object($result)) {
    $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
    _com2vb_delete_thread($comment);
  }
}
