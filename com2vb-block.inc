<?php

/**
 * @file
 * Set of functions for blocks.
 *
 */

/**
 * Implementation of hook_block().
 *
 * Generates a block with the most recent comments.
 */
function com2vb_block($op = 'list', $delta = 0, $edit = array()) {

  $deltas = explode('_', $delta);
  $block_var = 'com2vb_block_'. $delta;
  
  if ($op == 'list') {
    $blocks[0]['info'] = t('Recent comments');
	$blocks['disc_1']['info'] = t('Com2vb: Top discussions in forum 1');
	$blocks['disc_2']['info'] = t('Com2vb: Top discussions in forum 2');
	$blocks['views_1']['info'] = t('Com2vb: Hot topics in forum 1');
	$blocks['views_2']['info'] = t('Com2vb: Hot topics in forum 2');
	$blocks['last_1']['info'] = t('Com2vb: Last replies in forum 1');
	$blocks['last_2']['info'] = t('Com2vb: Last replies in forum 2');
	$blocks['online_1']['info'] = t('Com2vb: Users in forum online');
	$blocks['posters_1']['info'] = t('Com2vb: Top posters in forum');
    return $blocks;
  }
  else if ($op == 'view' && user_access('access comments')) {
  	switch ($deltas[0]) {
		case 'disc':
    		$block['subject'] = t('Com2vb: Top discussions in forum '). $deltas[1];
    		$block['content'] = theme('com2vb_block_forum', $deltas);
		break;
		
		case 'views':
			$block['subject'] = t('Com2vb: Hot topics in forum '). $deltas[1];
			$block['content'] = theme('com2vb_block_forum', $deltas);
		break;

		case 'last':
			$block['subject'] = t('Com2vb: Last replies in forum '). $deltas[1];
			$block['content'] = theme('com2vb_block_forum', $deltas);
		break;
		
		case 'online':
			$block['subject'] = t('Com2vb: Users in forum online '). $deltas[1];
			$block['content'] = theme('com2vb_block_forum_users', $deltas);
		break;
		
		case 'posters':
			$block['subject'] = t('Com2vb: Top posters in forum '). $deltas[1];
			$block['content'] = theme('com2vb_block_forum_users', $deltas);
		break;

		default:
    		$block['subject'] = t('Recent comments');
    		$block['content'] = theme('com2vb_block');
	}
	
    return $block;
  }
  else if ($op == 'configure') {
  	return _com2vb_block_settings_form($deltas, $block_var);
  }
  else if ($op == 'save') {
  			variable_set($block_var, $edit['com2vb_block_forum_list']);
			variable_set($block_var. '_history', $edit['com2vb_block_forum_period']);
			variable_set($block_var. '_limit', $edit['com2vb_block_items_limit']);
  }
}

/**
 * Returns a formatted list of recent comments to be displayed in the comment
 * block.
 *
 * @ingroup themeable
 */
function theme_com2vb_block() {
  $drupal_ver = substr(VERSION, 0, 1);

  $items = array();
  foreach (com2vb_get_recent(variable_get('com2vb_block_0_limit', 10)) as $comment) {
  	switch($drupal_ver) {
		case 5:
    	$items[] = l($comment->subject, 'node/'. $comment->nid, NULL, NULL, 'comment-'. $comment->cid). '<br />'. t('@time ago', array('@time' => format_interval(time() - $comment->timestamp)));
		break;
		
		case 6:
		$items[] = l($comment->subject, 'node/'. $comment->nid, array('fragment' => 'comment-'. $comment->cid)) .'<br />'. t('@time ago', array('@time' => format_interval(time() - $comment->timestamp)));
		break;
		
		case 7:
		$items[] = l($comment->subject, 'comment/' . $comment->cid, array('fragment' => 'comment-' . $comment->cid)) . '<br />' . t('@time ago', array('@time' => format_interval(REQUEST_TIME - $comment->changed)));
	}
  }
  if ($items) {
    return theme('item_list', $items);
  }
}

/**
 * Find topics in forum
 */
function com2vb_block_forum($block_type = array(), $period = '-30 day') {

	$forums = array();
	$block = $block_type[0];
	$block_var = 'com2vb_block_'. $block_type[0]. '_'. $block_type[1];
	$blocks = variable_get($block_var, FALSE);
	$limit = variable_get($block_var. '_limit', 10);
	$period = variable_get($block_var. '_history', $period);
	
	foreach($blocks as $key => $value) {
		if ($value) {
			$forums[] = $value;
		}
	}
	
	if ($blocks) {
		switch ($block) {
			case 'disc':
				$result = db_query("SELECT threadid, title, replycount FROM ". _vb_prefix(). "thread WHERE forumid IN (". implode(',', $forums). ") AND lastpost >= UNIX_TIMESTAMP(DATE_ADD(NOW(), interval %s)) AND open = 1 AND visible = 1 ORDER BY replycount DESC, lastpost DESC LIMIT 0, %d", $period, $limit);
			break;
		
			case 'views':
				$result = db_query("SELECT threadid, title FROM ". _vb_prefix(). "thread WHERE forumid IN (". implode(',', $forums). ") AND lastpost >= UNIX_TIMESTAMP(DATE_ADD(NOW(), interval %s)) AND visible = 1 ORDER BY views DESC, lastpost DESC LIMIT 0, %d", $period, $limit);
			break;
			
			case 'last':
				$result = db_query("SELECT threadid, title FROM ". _vb_prefix(). "thread WHERE forumid IN (". implode(',', $forums). ") AND visible = 1 ORDER BY lastpost DESC LIMIT 0, %d", $limit);
		}
	
		$threads = array();
		while ($topic = db_fetch_object($result)) {
			$threads[] = $topic;
		}
		return $threads;
	}
}

/**
 * Theme engine for com2vb_block_forum()
 */
function theme_com2vb_block_forum($block_type = FALSE) {
	if ($block_type) {
		$items = array();
		foreach (com2vb_block_forum($block_type) as $topic) {
			$items[] = '<a href='. variable_get('com2vb_forum_url', COM2VB_FORUM_URL). 'showthread.php?goto=newpost&t='. $topic->threadid. '>'. $topic->title. '</a>';
		}
		if ($items) {
			return theme('item_list', $items);
		}
	}
}

/**
 * Supply for blocks with users from forum
 */
function com2vb_block_forum_users($block_type = array()) {

	$block = $block_type[0];
	$block_var = 'com2vb_block_'. $block_type[0]. '_'. $block_type[1];
	$limit = variable_get($block_var. '_limit', 10);
	
		switch ($block) {
			case 'online':
				$result = db_query('SELECT s.userid, u.username FROM '. _vb_prefix(). 'session s, '. _vb_prefix(). 'user u WHERE u.userid = s.userid ORDER BY s.lastactivity DESC LIMIT 0, %d', $limit);
			break;
			
			case 'posters':
				$result = db_query('SELECT u.userid, u.username FROM '. _vb_prefix(). 'user u LEFT JOIN '. _vb_prefix(). 'userban b ON b.userid = u.userid WHERE b.userid IS NULL OR FROM_UNIXTIME(b.liftdate) < NOW() ORDER BY u.posts DESC LIMIT 0, %d', $limit);
			break;
		}

		$users = array();
		while ($users_online = db_fetch_object($result)) {
			$users[] = $users_online;
		}
		return $users;
}

/**
 *
 */
function theme_com2vb_block_forum_users($block_type = FALSE) {
	if ($block_type) {
		$items = array();
		foreach (com2vb_block_forum_users($block_type) as $users) {
			$items[] = '<a href='. variable_get('com2vb_forum_url', COM2VB_FORUM_URL). 'member.php?u='. $users->userid. '>'. $users->username. '</a>';
		}
		if ($items) {
			return theme('item_list', $items);
		}
	}
}

/**
 * Helper function
 */
function _com2vb_block_settings_form($deltas = array(), $block_var = NULL) {
  	switch ($deltas[0]) {
		case 'disc':
		case 'views':
		case 'last':
  			$forums = array();
			$forum = _vb_forum_list();
			foreach ($forum as $group) {
				foreach ($group as $key => $value) {
					$forums[$key] = $value;
				}
			}
			$form = array();
			$form['com2vb'] = array(
				'#type' => 'fieldset',
				'#title' => t('Forum settings'),
				'#description' => t("Select forums for displaying in block"),
				'#collapsible' => TRUE,
				'#collapsed' => FALSE,
			);
			$form['com2vb']['com2vb_block_forum_list'] = array(
				'#type' => 'checkboxes',
				'#default_value' => variable_get($block_var, 1),
				//'#title' => t('Create start post in forum'),
				//'#description' => t('Comment for checkboxes'),
				'#options' => $forums,
				);
			$form['com2vb']['com2vb_block_forum_period'] = array(
				'#type' => 'radios',
				'#title' => t('Time interval for selection'),
				'#options' => array(
					'-24 hour' => t('Last 24 hours'),
					'-48 hour' => t('Last 48 hours'),
					'-1 week' => t('7 days'),
					'-2 week' => t('2 weeks'),
					'-1 month' => t('1 month'),
					'-2 month' => t('2 monthes'),
					'-6 month' => t('6 monthes'),
					'-1 year' => t('12 monthes')
					),
				'#default_value' => variable_get($block_var. '_history', '-1month'),
			);
		
	}
	$form['com2vb']['com2vb_block_items_limit'] = array(
		'#type' => 'textfield',
		'#title' => 'Number of items for displaying',
		'#default_value' => variable_get($block_var. '_limit', 10),
	);
	return $form;
}