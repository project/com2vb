<?php

/**
 * @file
 * User page callbacks for the com2vb module.
 */

/**
 * Form builder; generate a comment editing form.
 *
 * @param $cid
 *   ID of the comment to be edited.
 * @ingroup forms
 */
function com2vb_edit($cid) {
  global $user;
    drupal_access_denied();
}

/**
 * This function is responsible for generating a comment reply form.
 * There are several cases that have to be handled, including:
 *   - replies to comments
 *   - replies to nodes
 *   - attempts to reply to nodes that can no longer accept comments
 *   - respecting access permissions ('access comments', 'post comments', etc.)
 *
 * The node or comment that is being replied to must appear above the comment
 * form to provide the user context while authoring the comment.
 *
 * @param $node
 *   Every comment belongs to a node. This is that node.
 *
 * @param $pid
 *   Some comments are replies to other comments. In those cases, $pid is the parent
 *   comment's cid.
 *
 * @return
 *   The rendered parent node or comment plus the new comment form.
 */
function com2vb_reply($node, $pid = NULL) {

  // Load the parent node.
//  $node = node_load($nid);
  //Load the parent thread in vBulletin
  $threadid = db_result(db_query('SELECT threadid FROM '._vb_prefix().'post WHERE postid = %d', $pid));

  // Set the breadcrumb trail.
  drupal_set_breadcrumb(array(l(t('Home'), NULL), l($node->title, 'node/'. $node->nid)));
  $op = isset($_POST['op']) ? $_POST['op'] : '';

  $output = '';

  if (user_access('access comments')) {
    // The user is previewing a comment prior to submitting it.
    if ($op == t('Preview')) {
      if (user_access('post comments')) {
        $output .= com2vb_form_box(array('pid' => $pid, 'nid' => $node->nid), NULL);
      }
      else {
        drupal_set_message(t('You are not authorized to post comments.'), 'error');
        drupal_goto("node/$node->nid");
      }
    }
    else {
      // $pid indicates that this is a reply to a comment.
      if ($pid) {
        // load the comment whose cid = $pid
        if ($comment = db_fetch_object(db_query('SELECT vb.dr_nid as nid, p.username as name, p.title as subject, p.pagetext as comment, u.uid, u.name AS registered_name, u.picture, u.data, p.threadid FROM {vb_thread} vb LEFT JOIN '._vb_prefix().'post p ON p.threadid = vb.vb_thread LEFT JOIN {users} u ON u.uid = p.userid WHERE p.postid = %d AND p.visible = %d AND vb.vb_thread  = p.threadid', $pid, !COMMENT_PUBLISHED))) {
          // If that comment exists, make sure that the current comment and the parent comment both
          // belong to the same parent node.
          if ($comment->nid != $node->nid) {
            // Attempting to reply to a comment not belonging to the current nid.
            drupal_set_message(t('The comment you are replying to does not exist.'), 'error');
            drupal_goto("node/$node->nid");
          }
          // Display the parent comment
          $comment = drupal_unpack($comment);
          $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
          $output .= theme('com2vb_view', $comment, $node);
		  //Create "re: subject" and quoted comment for easy replying
		  $recomment = '[quote]'.$comment->comment.'[/quote]';
		  $resubject = "Re: ";
		  $i = strpos($comment->subject, $resubject);
		  if ($i !== 0) {
		  	$resubject .= $comment->subject;
			if (strlen($resubject) > 64){
				$resubject = substr($resubject, 0, 64);
				}
			}
			else {
			$resubject = $comment->subject;
			}
        }
        else {
          drupal_set_message(t('The comment you are replying to does not exist.'), 'error');
          drupal_goto("node/$node->nid");
        }
      }
      // This is the case where the comment is in response to a node. Display the node.
      else if (user_access('access content')) {
        $output .= node_view($node);
	  	$resubject = 'Re: '.$node->title;
		if (strlen($resubject) > 64) {
			$resubject = substr($resubject, 0, 64);
			}
      }

      // Should we show the reply box?
      if (node_comment_mode($node->nid) != COMMENT_NODE_READ_WRITE) {
        drupal_set_message(t("This discussion is closed: you can't post new comments."), 'error');
        drupal_goto("node/$node->nid");
      }
      else if (user_access('post comments')) {
        $output .= com2vb_form_box(array('pid' => $pid, 'nid' => $node->nid, 'subject' => $resubject, 'comment' => $recomment), t('Reply'));
      }
      else {
        drupal_set_message(t('You are not authorized to post comments.'), 'error');
        drupal_goto("node/$node->nid");
      }
    }
  }
  else {
    drupal_set_message(t('You are not authorized to view comments.'), 'error');
    drupal_goto("node/$node->nid");
  }

  return $output;
}
