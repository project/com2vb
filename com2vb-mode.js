
if (Drupal.jsEnabled) {

  $(document).ready(function() {

    if ($("#edit-com2vb-forum-chk").size() && $("#edit-com2vb-forum-chk").attr("checked")) {

      $("#edit-com2vb-forum").attr("disabled","disabled");
      $("//#edit-com2vb-forum ~ div[@class=description]").hide(0);

    }

    if ($(".comment-mode input:radio").filter(":checked").val() == "0") {

		$("#edit-com2vb-forum-chk").attr("disabled","disabled");
		$("#edit-com2vb-forum").attr("disabled","disabled");
		$("#edit-com2vb-start-post").removeAttr("checked");
		$("#edit-com2vb-start-post").attr("disabled","disabled");

		}

    $("#edit-com2vb-forum-chk").bind("click", function() {

      if ($("#edit-com2vb-forum-chk").attr("checked")) {

        $("#edit-com2vb-forum").attr("disabled","disabled");
        $("//#edit-com2vb-forum ~ div[@class=description]").slideUp('slow');

      }

      else {

        $("#edit-com2vb-forum").removeAttr("disabled");
        $("#edit-com2vb-forum")[0].focus();
        $("//#edit-com2vb-forum ~ div[@class=description]").slideDown('slow');

      }

    });

    $(".comment-mode input:radio").bind("change", function() {

     com_mode = $(this).filter(":checked").val();
		  
		  switch (com_mode) {
			  case "0":
			  $("#edit-com2vb-forum-chk").attr("disabled","disabled");
			  $("#edit-com2vb-forum").attr("disabled","disabled");
			  $("#edit-com2vb-start-post").removeAttr("checked");
			  $("#edit-com2vb-start-post").attr("disabled","disabled");
			  break;
			  
			  case "1":
			  $("#edit-com2vb-forum-chk").removeAttr("disabled");
			  $("#edit-com2vb-start-post").attr("checked", "checked");
  			  $("#edit-com2vb-start-post").attr("disabled", "disabled");
			  break;
			  
			  default:
			  $("#edit-com2vb-forum-chk").removeAttr("disabled");
			  $("#edit-com2vb-start-post").removeAttr("disabled");
		  } 
    });
});

} 